<!DOCTYPE html>
<html>
<?php
	include("htmlsurce/_head.php");
	include("php/session.php")
?>
<body class="start-body">
<section class="normal-container">
	<section id="login-links" class="start-login">
	<a class="start-login-login" href="login.php">login</a>
	<a class="start-login-register" href="register.php">Register</a>
	</section>
	<section id="login-name" class="start-login">
		<p class="start-login-username">Welcome back <?php echo $_SESSION["username"]; ?><p>
	</section>
	<section class="start-links">
		<article class="start-links-right start-links-box">
			<p class="start-links-box-icon"><a href="index.php"><i class="fa fa-home" aria-hidden="true"></i></a></p>
		</article>
		<article class="start-links-left start-links-box">
			<p class="start-links-box-icon"><a href="forum.php"><i class="fa fa-comments" aria-hidden="true"></i></a></p>
		</article>
	<div id="login-panels">
		<article class="start-links-right start-links-box">
			<p class="start-links-box-icon"><a href="logout.php"><i class="fa fa-sign-out" aria-hidden="true"></i></a></p>
		</article>
		<article class="start-links-left start-links-box">
			<p class="start-links-box-icon"><a href="usersetting.php"><i class="fa fa-wrench" aria-hidden="true"></i></a></p>
		</article>
	</div>
</section>
<?php
	include("htmlsurce/_footer.php")
?>
<script>
	var login = <?php echo $_SESSION['login']?>;
	if (login == false)
	{
		document.getElementById('login-name').style.display = 'none';
		document.getElementById('login-panels').style.display = 'none';
	}
	else if (login == true)
	{
		document.getElementById('login-name').style.display = 'block';
		document.getElementById('login-panels').style.display = 'block';
		document.getElementById('login-links').style.display = 'none';
	}
</script>
</body>
</html>